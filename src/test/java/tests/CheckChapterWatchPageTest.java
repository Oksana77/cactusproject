package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.ChapterWatches;
import pages.HomePage;

import java.util.concurrent.TimeUnit;

public class CheckChapterWatchPageTest extends BaseTest {

    @Test(groups = {"positiveCase"}, description = "Going to the page category \"clock\"")
    public void testLogMail (){
        driver.get("https://cactus.kh.ua");
        HomePage log = new HomePage(driver);
        ChapterWatches result = new ChapterWatches(driver);
        log.clickChapterWatches();
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(result.getChapterName().contains("Умные Часы"));
    }
}
