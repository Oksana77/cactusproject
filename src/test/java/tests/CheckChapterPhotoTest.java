package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.ChapterPhoto;
import pages.HomePage;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertTrue;

public class CheckChapterPhotoTest extends BaseTest {

    @Test(groups = {"positiveCase"}, description = "Going to the page category \"photo and video\"")
    public void testLogMail (){
        driver.get("https://cactus.kh.ua");
        HomePage log = new HomePage(driver);
        ChapterPhoto result = new ChapterPhoto(driver);
        log.clickChapterPhoto();
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(result.getChapterName().contains("Фото, видео и ТВ"));
    }
}
