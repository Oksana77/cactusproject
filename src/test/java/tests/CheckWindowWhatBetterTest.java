package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;

import java.util.concurrent.TimeUnit;

public class CheckWindowWhatBetterTest extends BaseTest {

    @Test(groups = {"positiveCase"}, description = "Work move to better button")
    public void testLogMail() {
        driver.get("https://cactus.kh.ua");

        HomePage log = new HomePage(driver);
        HomePage result = new HomePage(driver);
        log.moveToBetterButton();
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(result.getWhatWindowBetterText().contains("Каждый день хотим вас радовать!"));
    }
}
