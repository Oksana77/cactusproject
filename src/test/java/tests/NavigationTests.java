package tests;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.Header;
import pages.SectionWarranty;
import pages.ShopCactus;

public class NavigationTests extends BaseTest {

    @Test(groups = {"positiveCase"}, description = "Return to top of page")
    public void verifyThatScrollUpButtonNavigateToTopOfPage() {
        Header header = new Header(driver);
        ShopCactus shopCactusPage = new Header(driver).navigateToShopCactus();
        //scroll down
        shopCactusPage.scrollDown();

        //click on the button
        shopCactusPage.clickScrollUpButton();

        Assert.assertTrue(header.basketIcon.isDisplayed(), "basket icon is displayed");
    }

    @Test(groups = {"positiveCase"}, description = "Came back on main page")
    public void cameBackMainPage() {
        driver.get("https://cactus.kh.ua/content/6-garantiya");
        Header header = new Header(driver);
        SectionWarranty cameBackButton = new SectionWarranty(driver).clickCameBackButton();

        Assert.assertTrue(header.logoCactus.isDisplayed(),
                "Result was found");

    }

    @Test(groups = {"positiveCase"}, description = "Came back on main page")
    public void cameBackMainPage1() {
        driver.get("https://cactus.kh.ua/content/4-dostavka");
        Header header = new Header(driver);
        SectionWarranty cameBackButton = new SectionWarranty(driver).clickCameBackButton();

        Assert.assertTrue(header.logoCactus.isDisplayed(),
                "Result was found");
    }
}
