package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.CompareButton;
import pages.LogPage;

import java.util.concurrent.TimeUnit;

public class CheckCompareTest extends BaseTest {

    @Test(groups = {"positiveCase"}, description = "Going to the comparison page from the login page")
    public void testLogMail (){
        driver.get("https://cactus.kh.ua/authentication");
        LogPage log = new LogPage(driver);
        CompareButton result = new CompareButton(driver);
        log.clickCompare();
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(result.getNoGoods().contains("Вы не добавили для сравнения ни одного товара"));
    }
}
