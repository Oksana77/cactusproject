package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.ChapterSmartHouse;
import pages.HomePage;

import java.util.concurrent.TimeUnit;

public class CheckChapterSmartHouseTest extends BaseTest {

    @Test(groups = {"positiveCase"}, description = "Going to the page category \"smart house\"")
    public void testLogMail (){
        driver.get("https://cactus.kh.ua");
        HomePage log = new HomePage(driver);
        ChapterSmartHouse result = new ChapterSmartHouse(driver);
        log.clickChapterSmartHouse();
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
       Assert.assertTrue(result.getChapterName().contains("Умный дом"));
    }
}
