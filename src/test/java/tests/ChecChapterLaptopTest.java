package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.ChapterLaptop;
import pages.HomePage;

import java.util.concurrent.TimeUnit;

public class ChecChapterLaptopTest extends BaseTest{

    @Test(groups = {"positiveCase"}, description = "Going to the page category \"laptops\"")
    public void testLogMail (){
        driver.get("https://cactus.kh.ua");
        HomePage log = new HomePage(driver);
        ChapterLaptop result = new ChapterLaptop(driver);
        log.clickChapterLapTops();
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(result.getChapterName().contains("Планшеты, ноутбуки и ПК"));
    }
}
