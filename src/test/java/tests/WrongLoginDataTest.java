package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.LogPage;

import java.util.concurrent.TimeUnit;

public class WrongLoginDataTest extends BaseTest {

    @Test(groups = {"negativeCase"}, description = "Entering the wrong E-mail address and password")
    public void testLogMail1() {
        driver.get("https://cactus.kh.ua/authentication");
        LogPage log = new LogPage(driver);
        LogPage result = log.loginSetMail("Good@Mail.String");
        result = log.loginSetPass("WrongPass");
        result = log.clickLogIn();
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(log.fieldText.isDisplayed(), "Ошибка авторизации");
    }

}
