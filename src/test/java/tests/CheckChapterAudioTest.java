package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.ChapterAudio;
import pages.HomePage;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertTrue;

public class CheckChapterAudioTest extends BaseTest {

    @Test(groups = {"positiveCase"}, description = "Going to the page category \"audio\"")
    public void testLogMail (){
        driver.get("https://cactus.kh.ua");
        HomePage log = new HomePage(driver);
        ChapterAudio result = new ChapterAudio(driver);
        log.clickChapterAudio();
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(result.getChapterName().contains("Аудио"));
    }
}
