package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.ChapterBettery;
import pages.HomePage;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertTrue;

public class CheckCapterBatteryTest extends BaseTest {

    @Test(groups = {"positiveCase"}, description = "Going to the page category \"bettery\"")
    public void testLogMail (){
        driver.get("https://cactus.kh.ua");
        HomePage log = new HomePage(driver);
        ChapterBettery result = new ChapterBettery(driver);
        log.clickChapterBattery();
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(result.getChapterName().contains("Портативные батареи"));
    }
}
