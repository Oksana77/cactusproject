package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.ChapterFun;
import pages.HomePage;

import java.util.concurrent.TimeUnit;

public class CheckChapterFunsTest extends BaseTest {

    @Test(groups = {"positiveCase"}, description = "Going to the page category \"fun\"")
    public void testLogMail (){
        driver.get("https://cactus.kh.ua");
        HomePage log = new HomePage(driver);
        ChapterFun result = new ChapterFun(driver);
        log.clickChapterFuns();
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(result.getChapterName().contains("Развлечения"));
    }
}
