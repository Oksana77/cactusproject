package tests;


import io.qameta.allure.Attachment;

import org.openqa.selenium.*;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.Header;
import pages.SearchResult;


public class SearchTests extends BaseTest {


    @DataProvider
    public Object[][] getData() {
        return new Object[][]{
                {"смартфоны"},
                {"умные часы"},
                {"фото"},
                {"45"},
                {"S9"},
        };
    }

    @Test(groups = {"positiveCase"}, dataProvider = "getData", description = "Positive search cases")
    public void executeSearch(String inputText) {
        driver.get("https://cactus.kh.ua/");

        SearchResult searchResult = new Header(driver)
                .enterInputText(inputText)
                .performSearch();

        /*  takeScreenshot(driver,"pageScreenshot");*/


        Assert.assertTrue(searchResult.searchResultFindText.getText().contains(inputText),
                "Result was found");
        Assert.assertTrue(searchResult.singleSearchResult.isDisplayed(),
                "Some result was found");


    }

    @Attachment(value = "{1}", type = "image/png")
    public byte[] takeScreenshot(WebDriver driver, String screenshotName) {
        byte[] result = null;
        if (driver != null) {
            result = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
        }
        return result;
    }

    @Test(groups = {"negativeCase"}, description = "Negative search cases")
    public void negativeSearch() {
        driver.get("https://cactus.kh.ua/");

        SearchResult searchResult = new Header(driver)
                .enterInputText("dfdsfsdfsdfsdfdsfewfsf")
                .performSearch();

        Assert.assertTrue(searchResult.noResultText.isDisplayed(),
                "Result was NOT found");
    }


}

