package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.ChapterAcsesuares;
import pages.HomePage;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertTrue;

public class CheckChapterAcsesuaresTest extends BaseTest {

    @Test(groups = {"positiveCase"}, description = "Going to the page category \"acsesuares\"")
    public void testLogMail (){
        driver.get("https://cactus.kh.ua");
        HomePage log = new HomePage(driver);
        ChapterAcsesuares result = new ChapterAcsesuares(driver);
        log.clickChapterAcsesuares();
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
       Assert.assertTrue(result.getChapterName().contains("Аксессуары"));
    }
}
