package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.ChapterSmartPhones;
import pages.HomePage;

import java.util.concurrent.TimeUnit;

public class CheckChapterSmartPhonesTest extends BaseTest {

    @Test(groups = {"positiveCase"}, description = "Going to the page category \"smartphones\"")
    public void testLogMail (){
        driver.get("https://cactus.kh.ua");
        HomePage log = new HomePage(driver);
        ChapterSmartPhones result = new ChapterSmartPhones(driver);
        log.clickChapterSmartPhones();
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(result.getChapterName().contains("Смартфоны"));
    }
}
