package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.LogPage;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertTrue;

public class WrongMailLoginTest extends BaseTest {

    @Test(groups = {"negativeCase"}, description = "Entering the wrong E-mail address")
    public void testLogMail (){
        driver.get("https://cactus.kh.ua/authentication");
        LogPage log = new LogPage(driver);
        LogPage result = log.loginSetMail("WrongmailString");
        result = log.clickLogIn();
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(log.fieldText.isDisplayed(),"Неверный адрес e-mail");
    }


}
