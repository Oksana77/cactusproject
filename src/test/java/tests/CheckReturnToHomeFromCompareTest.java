package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.CompareButton;
import pages.HomePage;

import java.util.concurrent.TimeUnit;

public class CheckReturnToHomeFromCompareTest extends BaseTest {

    @Test(groups = {"positiveCase"}, description = "Return to home page from compare page")
    public void testLogMail (){
        driver.get("https://cactus.kh.ua/products-comparison");
        CompareButton log = new CompareButton(driver);
        HomePage result = new HomePage(driver);
        log.clickReturnToShop();
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(result.getPageDescription().contains("Cactus - магазин гаджетов и аксессуаров"));
    }
}
