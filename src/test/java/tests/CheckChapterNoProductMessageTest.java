package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;

import java.util.concurrent.TimeUnit;

public class CheckChapterNoProductMessageTest extends BaseTest {

    @Test(groups = {"positiveCase"}, description = "message that the cart is empty")
    public void testLogMail (){
        driver.get("https://cactus.kh.ua");
        HomePage log = new HomePage(driver);
        HomePage result = log.clickCart();
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(result.getNoProductsMessage().contains("Нет товаров в корзине"));
    }
}
