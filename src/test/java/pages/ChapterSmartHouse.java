package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ChapterSmartHouse extends BasePage {
    public ChapterSmartHouse(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(id="title")
    public WebElement chapterName;

    @Step
    public String getChapterName() {
        return chapterName.getText();
    }
}
