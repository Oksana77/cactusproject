package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ShopCactus extends BasePage {

    public ShopCactus(WebDriver webDriver) {
        super(webDriver);

    }

    @FindBy(xpath = "//a[@id=\"scrollUp\"]")
    WebElement scrollUpButton;

    @Step
    public void scrollDown() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
    }

    @Step

    public void clickScrollUpButton() {
        scrollUpButton.click();


    }
}
