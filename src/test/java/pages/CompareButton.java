package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CompareButton extends BasePage {
    public CompareButton(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(className = "return-to-shop")
    WebElement returnButton;

    @FindBy(className = "lead-title")
    WebElement windowNoGoods;

    @Step

    public String getNoGoods() {
        return windowNoGoods.getText();
    }

    @Step

    public CompareButton clickReturnToShop() {
        returnButton.click();
        return new CompareButton(driver);
    }
}
