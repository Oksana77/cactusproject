package pages;


import io.qameta.allure.Step;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class Header extends BasePage {


    @FindBy(xpath = "//input[@class=\"search_query form-control grey ac_input\"]")
    WebElement searhField;

    @FindBy(className = "basket-item-count")
    public WebElement basketIcon;

    @FindBy(xpath ="//div[@class=\"row\"]//a/img[@class= \"logo\"]" )
    public WebElement logoCactus;

    public Header(WebDriver driver) {
        super(driver);
    }

    @Step
    public Header enterInputText(String inputText) {
        searhField.sendKeys(inputText);
        return this;
    }

    @Step
    public SearchResult performSearch() {
        searhField.sendKeys(Keys.ENTER);
        return new SearchResult(driver);
    }

    @Step
    public ShopCactus navigateToShopCactus() {
        driver.get("https://cactus.kh.ua/content/1-cactus");
        return new ShopCactus(driver);
    }


}
