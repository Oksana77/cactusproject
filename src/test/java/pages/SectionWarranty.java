package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SectionWarranty extends BasePage {

    public SectionWarranty(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "//div[@class=\"wpb_wrapper\"]/div[@class=\"header-inner-logo\"]/a")
    WebElement cameBackButton;

    public SectionWarranty clickCameBackButton() {
        cameBackButton .click();
        return new SectionWarranty(driver);
    }
}
