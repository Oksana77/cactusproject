package pages;


import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LogPage extends BasePage {

    @FindBy(xpath = "//*[@id='center_column']/div[1]//li")
    public WebElement fieldText;

    @FindBy(id = "email_create")
    public WebElement fieldE_mailCreate;

    @FindBy(id = "SubmitCreate")
    public WebElement submitCreate;

    @FindBy(id = "email")
    public WebElement fieldLogin;

    @FindBy(id = "passwd")
    public WebElement fieldPass;

    @FindBy(id = "SubmitLogin")
    public WebElement submitLogin;

    public LogPage(WebDriver driver) {
        super(driver);
    }

    @Step

    public LogPage registerSetMail(String mailStr) {
        fieldE_mailCreate.sendKeys(mailStr);
        submitCreate.click();
        return new LogPage(driver);
    }

    @Step
    public LogPage clickRegister() {
        submitCreate.click();
        return new LogPage(driver);
    }


    @Step

    public LogPage loginSetMail(String mailStr) {
        fieldLogin.sendKeys(mailStr);
        return new LogPage(driver);
    }

    @Step

    public LogPage loginSetPass(String mailStr) {
        fieldPass.sendKeys(mailStr);
        return new LogPage(driver);
    }

    @Step

    public LogPage clickLogIn() {
        submitLogin.click();
        return new LogPage(driver);
    }


    @Step

    public LogPage clickCompare() {
        driver.findElement(By.className("compare-header")).click();
        return new LogPage(driver);
    }


}
