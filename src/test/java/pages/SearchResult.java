package pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchResult extends BasePage {

    public SearchResult(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "//*[contains(@class,'product-listing')]")
    public WebElement searchResultFindText;

    @FindBy(xpath = "//div[@id=\"center_column\"]/h1")
    public WebElement singleSearchResult;

    @FindBy(xpath = "//*[@id='columns']//p[contains(@class,'alert-warning')]")
    public WebElement noResultText;


    public Header getHeader() {
        return new Header(driver);
    }


}
