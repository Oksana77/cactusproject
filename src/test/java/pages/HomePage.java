package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.By.className;

public class HomePage extends BasePage {
    public HomePage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(className=("circle-what-will-be-better"))
    public WebElement betterButton;

    @FindBy(linkText=("Часы"))
    public WebElement chapterWatches;

    @FindBy(linkText=("Умный дом"))
    public WebElement chapterSmartHouse;

    @FindBy(linkText=("Смартфоны"))
    public WebElement chapterSmartPhones;

    @FindBy(className=("basket-item-count"))
    public WebElement cart;

    @FindBy(className=("cart_block_no_products"))
    public WebElement clickCart;

    @FindBy(linkText=("Развлечения"))
    public WebElement chapterFuns;

    @FindBy(linkText=("Аудио"))
    public WebElement chapterAudio;

    @FindBy(linkText=("Фитнес и здоровье"))
    public WebElement chapterFitnes;

    @FindBy(linkText=("Аксессуары"))
    public WebElement chapterAcsesuares;

    @FindBy(linkText=("Фото и видео"))
    public WebElement chapterPhoto;

    @FindBy(linkText=("Батареи"))
    public WebElement chapterBattery;

    @FindBy(linkText=("Планшеты, ноутбуки"))
    public WebElement chapterLapTops;


    @Step

    public String getPageDescription() {
        return driver.findElement(className("cat_desc")).getText();
    }
    @Step

    public String getNoProductsMessage() {
        return clickCart.getText();
    }

    @Step

    public HomePage clickCart() {
        cart.click();
        return new HomePage(driver);
    }
    @Step

    public HomePage clickChapterSmartPhones() {
        chapterSmartPhones.click();
        return new HomePage(driver);
    }
    @Step

    public HomePage clickChapterLapTops() {
        chapterLapTops.click();
        return new HomePage(driver);
    }
    @Step

    public HomePage clickChapterWatches() {
        chapterWatches.click();
        return new HomePage(driver);
    }
    @Step

    public HomePage clickChapterFitnes() {
        chapterFitnes.click();
        return new HomePage(driver);
    }
    @Step

    public HomePage clickChapterSmartHouse() {
        chapterSmartHouse.click();
        return new HomePage(driver);
    }
    @Step

    public HomePage clickChapterBattery() {
        chapterBattery.click();
        return new HomePage(driver);
    }
    @Step

    public HomePage clickChapterFuns() {
        chapterFuns.click();
        return new HomePage(driver);
    }
    @Step

    public HomePage clickChapterPhoto() {
        chapterPhoto.click();
        return new HomePage(driver);
    }
    @Step

    public HomePage clickChapterAudio() {
        chapterAudio.click();
        return new HomePage(driver);
    }
    @Step

    public HomePage clickChapterAcsesuares() {
        chapterAcsesuares.click();
        return new HomePage(driver);
    }

    @Step

    public HomePage moveToBetterButton() {
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollBy(0,1000)", "");
        betterButton.click();
        return new HomePage(driver);
    }

    @Step

    public String getWhatWindowBetterText() {
        return driver.findElement(className("form-better")).getText();
    }


}
