package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ChapterSmartPhones extends BasePage {
    public ChapterSmartPhones(WebDriver webDriver) {
        super(webDriver);
    }
    public String getChapterName() {

        return driver.findElement(By.id("title")).getText();
    }
}
